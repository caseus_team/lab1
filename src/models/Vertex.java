package models;

import enums.Plane;
import transformation.Transformation;

public class Vertex {

    private double[] coordinates;

    public Vertex(double x, double y, double z) {
        coordinates = new double[4];
        coordinates[0] = x;
        coordinates[1] = y;
        coordinates[2] = z;
        coordinates[3] = 1;
    }

    public Point vertexToPoint(Plane plane) {
        switch (plane) {
            case XZ: return new Point(coordinates[0], coordinates[2]);
            case YZ: return new Point(coordinates[1], coordinates[2]);
            default: return new Point(coordinates[0], coordinates[1]);
        }
    }

    public Vertex(double[] coordinates) {
        this.coordinates = coordinates;
    }

    public Vertex doTransformAndGetNewVertex(Transformation transformation) {
        return new Vertex(transformation.doTransform(coordinates));
    }

    public void doTransform(Transformation transformation) {
        coordinates = transformation.doTransform(coordinates);
    }

    public double getX() {
        return coordinates[0];
    }

    public double getY() {
        return coordinates[1];
    }

    public double getZ() {
        return coordinates[2];
    }

    public double getH() {
        return coordinates[3];
    }

    public double[] getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(double[] coordinates) {
        this.coordinates = coordinates;
    }
}
