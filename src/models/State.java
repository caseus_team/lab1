package models;

import enums.Axis;
import enums.Plane;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class State {

    private Set<Axis> scale = new HashSet<>(Arrays.asList(Axis.values()));
    private Axis rotation = Axis.X;
    private Axis moving = Axis.X;
    private Plane projection = Plane.XY;
    private double rotationAngle = 0;

    public void addDiffToAngleValue(double value) {
        rotationAngle += value;
        rotationAngle %= Math.PI;
        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! " + rotationAngle);
    }

    public double getRotationAngle() {
        return rotationAngle;
    }

    public void setRotationAngle(double rotationAngle) {
        this.rotationAngle = rotationAngle;
    }

    public Set<Axis> getScale() {
        return scale;
    }

    public void setScale(Set<Axis> scale) {
        this.scale = scale;
    }

    public void scaleAction(Axis axis) {
        if (scale.contains(axis)) {
            scale.remove(axis);
        }
        else {
            scale.add(axis);
        }
    }

    public Axis getRotation() {
        return rotation;
    }

    public void setRotation(Axis rotation) {
        this.rotation = rotation;
    }

    public Axis getMoving() {
        return moving;
    }

    public void setMoving(Axis moving) {
        this.moving = moving;
    }

    public Plane getProjection() {
        return projection;
    }

    public void setProjection(Plane projection) {
        this.projection = projection;
    }
}
