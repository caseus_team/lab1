package models;

import enums.Plane;
import transformation.Transformation;

import java.awt.geom.Path2D;
import java.util.Arrays;

public class Polygon {

    private Vertex[] vertexes;

    public Polygon(Vertex ...vertex) {
        vertexes = new Vertex[vertex.length];
        System.arraycopy(vertex, 0, vertexes, 0, vertex.length);
    }

    public void transform(Transformation transformation) {
        Arrays.stream(vertexes).forEach(vertex -> {
//            System.out.println("---- " + Arrays.toString(vertex.getCoordinates()));
            vertex.doTransform(transformation);
//            System.out.println("++++ " + Arrays.toString(vertex.getCoordinates()));
        });
    }

    private double convertX(double x, double width) {
        return x + width / 2;
    }

    private double convertY(double y, double height) {
        return -y + height / 2;
    }

    public Path2D draw(double width, double height, Transformation transformation, Plane plane) {
        Path2D.Double path = new Path2D.Double();
        Point[] points =
                Arrays.stream(vertexes)
                        .map(vertex -> vertex.doTransformAndGetNewVertex(transformation))
                        .map(vertex -> vertex.vertexToPoint(plane))
                        .toArray(Point[]::new);
        path.moveTo(
                convertX(points[0].x, width),
                convertY(points[0].y, height)
        );
        for (int i = 1; i < points.length; i++) {
            path.lineTo(
                    convertX(points[i].x, width),
                    convertY(points[i].y, height)
            );
        }
        path.closePath();
        return path;
    }

    public Vertex[] getVertexes() {
        return vertexes;
    }
}
