package transformation.projecting;

import enums.Plane;
import transformation.Transformation;
import transformation.math.Operations;

public class PerspectiveProjection extends Transformation {

    private static double D = 1000;

    @Override
    public double[] multiply(double[] vector) {
        return Operations.multiply(matrix, vector);
    }

    public PerspectiveProjection(Plane plane) {
        super(new double[][] {
                {1, 0, 0, 0},
                {0, 1, 0, 0},
                {0, 0, 1, 0},
                {0, 0, 1/D, 1}
        });
        matrix[plane.ordinal()][plane.ordinal()] = 0;
    }

}
