package transformation.math;

public class Operations {

    public static double[][] multiply(double[][] a, double[][] b) {
        double[][] c = new double[a.length][b[0].length];
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < b[0].length; j++) {
                c[i][j] = 0;
                for (int k = 0; k < b.length; k++) {
                    c[i][j] += a[i][k] * b[k][j];
                }
            }
        }
        return c;
    }

    public static double[] multiply(double[][] matrix, double[] vector) {
        double[] result = new double[vector.length];
        for (int i = 0; i < matrix.length; i++) {
            result[i] = 0;
            for (int k = 0; k < vector.length; k++) {
                result[i] += matrix[i][k] * vector[k];
            }
        }
        return result;
    }

    public static double[] multiply(double[] vector, double[][] matrix) {
        double[] result = new double[vector.length];
        for (int j = 0; j < matrix[0].length; j++) {
            result[j] = 0;
            for (int r = 0; r < matrix.length; r++) {
                result[j] += vector[r] * matrix[r][j];
            }
        }
        return result;
    }

}
