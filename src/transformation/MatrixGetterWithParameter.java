package transformation;

public interface MatrixGetterWithParameter {
    
    double[][] getMatrix(double value);
    
}
