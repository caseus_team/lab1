package transformation;

public interface MatrixGetter {

    double[][] getMatrix();

}
