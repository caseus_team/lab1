package transformation;

import enums.Axis;

import java.util.EnumMap;
import java.util.Map;

import static java.lang.Math.cos;
import static java.lang.Math.sin;

public class Rotation extends Transformation {

    private static Map<Axis, MatrixGetterWithParameter> rotationMatrix = new EnumMap<>(Axis.class);

    static {
        rotationMatrix.put(Axis.X, Rotation::getRotationAroundX);
        rotationMatrix.put(Axis.Y, Rotation::getRotationAroundY);
        rotationMatrix.put(Axis.Z, Rotation::getRotationAroundZ);
    }

    public Rotation(Axis axis, double angle) {
        super(rotationMatrix.get(axis).getMatrix(angle));
    }

    private static double[][] getRotationAroundX(double angle) {
        return new double[][] {
                {1, 0, 0, 0},
                {0, cos(angle), sin(angle), 0},
                {0, -sin(angle), cos(angle), 0},
                {0, 0, 0, 1}
        };
    }

    private static double[][] getRotationAroundY(double angle) {
        return new double[][] {
                {cos(angle), 0, -sin(angle), 0},
                {0, 1, 0, 0},
                {sin(angle), 0, cos(angle), 0},
                {0, 0, 0, 1}
        };
    }

    private static double[][] getRotationAroundZ(double angle) {
        return new double[][] {
                {cos(angle), sin(angle), 0, 0},
                {-sin(angle), cos(angle), 0, 0},
                {0, 0, 1, 0},
                {0, 0, 0, 1}
        };
    }

}
