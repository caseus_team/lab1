package render;

import enums.Axis;
import enums.Plane;
import models.State;
import transformation.Moving;
import transformation.Rotation;
import transformation.Scale;
import transformation.Transformation;
import transformation.projecting.ParallelProjection;
import transformation.projecting.PerspectiveProjection;
import transformation.projecting.ProjectionOnPlane;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.PI;
import static java.lang.Math.sin;
import static java.lang.Thread.sleep;

public class Frame extends JFrame implements ActionListener, MouseWheelListener {

    public static int HEIGHT = 400;
    public static int WIDTH = 500;
    JPanel scene;
    private RenderPane renderPane;
    private State state = new State();

    public Frame() throws HeadlessException {
        super();
        Container contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());

        JLabel labelProjection = new JLabel("Проекция на плоскость:");
        labelProjection.setBounds(540, 10, 200, 20);
        add(labelProjection);
        createButtonGroupForPlane();

        createButtonGroupForProjection();

        JLabel labelRotation = new JLabel("Ось вращения:");
        labelRotation.setBounds(540, 110, 200, 20);
        add(labelRotation);
        createButtonGroupForRotation();

        JLabel labelScale = new JLabel("Ось масштабирования:");
        labelScale.setBounds(540, 150, 200, 20);
        add(labelScale);
        createButtonGroupForScale();

        JLabel labelMoving = new JLabel("Ось перемещения:");
        labelMoving.setBounds(540, 210, 200, 20);
        add(labelMoving);
        createButtonGroupForMoving();

        JLabel help = new JLabel("<html><center>Масштаб: <i>колесо мыши</i><br/>" +
                "Вращение: ← →<br/>" +
                "Перемещение: ↑ ↓</center></html>");
        help.setBounds(540, 270, 200, 100);
        add(help);

        this.setBackground(Color.WHITE);

        renderPane = new RenderPane(HEIGHT, WIDTH);

        scene = new JPanel() {
            @Override
            public void paint(Graphics g) {
                super.paint(g);
                renderPane.paintComponent(g);
            }
        };
        scene.setBackground(Color.WHITE);
        scene.setBounds(10, 10, WIDTH, HEIGHT - 10);
        scene.setBorder(BorderFactory.createLineBorder(Color.GRAY));
        this.add(scene);

        setSize(800, 400);
        setVisible(true);

        setLocationRelativeTo(null);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        addMouseWheelListener(this);
        KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(this::onKeyEvent);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String actionCommand = e.getActionCommand();
        if (actionCommand.startsWith("plane")) {
            state.setProjection(
                    Plane.valueOf(
                            actionCommand.substring("plane".length())
                    )
            );
            renderPane.setCurrentPlane(state.getProjection());
        }
        if (actionCommand.startsWith("perspective")) {
            renderPane.setProjection(new PerspectiveProjection(state.getProjection()));
        }
        if (actionCommand.startsWith("orto")) {
            renderPane.setProjection(new ProjectionOnPlane(state.getProjection()));
        }
        if (actionCommand.startsWith("koso")) {
            renderPane.setProjection(new ParallelProjection(state.getProjection()));
        }
        if(actionCommand.startsWith("axis")) {
            state.setRotation(
                    Axis.valueOf(
                            actionCommand.substring("axis".length())
                    )
            );
        }
        if(actionCommand.startsWith("scale")) {
            state.scaleAction(
                    Axis.valueOf(
                            actionCommand.substring("scale".length())
                    )
            );
        }
        if(actionCommand.startsWith("move")) {
            state.setMoving(
                    Axis.valueOf(
                            actionCommand.substring("move".length())
                    )
            );
        }
        repaint();
    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {
        doTransform(
                new Scale(
                        e.getWheelRotation() < 0 ? 1.1 : 0.9,
                        state.getScale().toArray(new Axis[]{})
                )
        );
        repaint();
    }

    public void doTransform(Transformation transformation) {
        renderPane.doTransform(transformation);
    }

    private boolean onKeyEvent(KeyEvent e) {
        if(e.getID() != KeyEvent.KEY_PRESSED)
            return false;
        switch(e.getKeyCode()) {
            case 37: {
                state.setRotationAngle(0.01);
                doTransform(new Rotation(state.getRotation(), state.getRotationAngle()));
                break;
            }
            case 39: {
                state.setRotationAngle(-0.01);
                doTransform(new Rotation(state.getRotation(), state.getRotationAngle()));
                break;
            }
            case 38: {
                doTransform(new Moving(state.getMoving(), 1));
                break;
            }
            case 40: {
                doTransform(new Moving(state.getMoving(), -1));
                break;
            }
            case 32: {
                jump();
                return true;
            }
        }

        repaint();
        return true;
    }

    private void jumpOnDiff(double value) {
        doTransform(new Moving(Axis.Y, value));
        scene.paintImmediately(0, 0, WIDTH, HEIGHT);
        try {
            sleep(70);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void jump() {
        List<Double> values = new ArrayList<>();
        double x, prev = 0;
        for (x = 0.1; x < PI / 2; x += 0.1) {
            values.add(sin(x) - sin(prev));
            prev = x;
        }
        values.add(sin(PI / 2) - sin(prev));
        for (double coeff = 100; coeff > 4; coeff /= 2) {
            for (Double value : values) {
                jumpOnDiff(coeff * value);
            }
            for (int i = values.size() - 1; i >= 0; i--) {
                jumpOnDiff(coeff * values.get(i) * -1);
            }
        }
        System.out.println("end");
    }

    private void createButtonGroupForMoving() {
        int y = 230, z = 36, h = 20;
        ButtonGroup buttonGroup = new ButtonGroup();
        buttonGroup.add(
                addRadioButton("X", "moveX", 540, y, z, h)
        );
        buttonGroup.getElements().nextElement().setSelected(true);
        buttonGroup.add(
                addRadioButton("Y", "moveY", 590, y, z, h)
        );
        buttonGroup.add(
                addRadioButton("Z", "moveZ", 640, y, z, h)
        );
    }

    private void createButtonGroupForScale() {
        int y = 170, z = 36, h = 20;
        addCheckBox("X", "scaleX", 540, y, z, h);
        addCheckBox("Y", "scaleY", 575, y, z, h);
        addCheckBox("Z", "scaleZ", 610, y, z, h);
    }

    private void createButtonGroupForRotation() {
        int y = 130, z = 36, h = 20;
        ButtonGroup buttonGroup = new ButtonGroup();
        buttonGroup.add(
                addRadioButton("X", "axisX", 540, y, z, h)
        );
        buttonGroup.getElements().nextElement().setSelected(true);
        buttonGroup.add(
                addRadioButton("Y", "axisY", 575, y, z, h)
        );
        buttonGroup.add(
                addRadioButton("Z", "axisZ", 610, y, z, h)
        );
    }

    private void createButtonGroupForPlane() {
        int y = 30, z = 40, h = 20;
        ButtonGroup buttonGroup = new ButtonGroup();
        buttonGroup.add(
                addRadioButton("YZ", "planeYZ", 540, y, z, h)
        );
        buttonGroup.getElements().nextElement().setSelected(true);
        buttonGroup.add(
                addRadioButton("XZ", "planeXZ", 590, y, z, h)
        );
        buttonGroup.add(
                addRadioButton("XY", "planeXY", 640, y, z, h)
        );
    }

    private void createButtonGroupForProjection() {
        int x = 540, z = 40, h = 20;
        ButtonGroup buttonGroup = new ButtonGroup();
        buttonGroup.add(
                addRadioButton("Ортографическая", "orto", x, 50, 150, h)
        );
        buttonGroup.getElements().nextElement().setSelected(true);
        buttonGroup.add(
                addRadioButton("Перспективная", "perspective", x, 70, 150, h)
        );
        buttonGroup.add(
                addRadioButton("Косоугольная", "koso", x, 90, 150, h)
        );
    }

    private void addCheckBox(String text, String action, int x, int y, int z, int h) {
        JCheckBox jCheckBox = new JCheckBox(text);
        jCheckBox.setActionCommand(action);
        jCheckBox.addActionListener(this);
        jCheckBox.setBounds(x, y, z, h);
        jCheckBox.setBackground(Color.WHITE);
        jCheckBox.setSelected(true);
        add(jCheckBox);
    }

    private JRadioButton addRadioButton(String text, String action, int x, int y, int z, int h) {
        JRadioButton jRadioButton = new JRadioButton(text);
        jRadioButton.setActionCommand(action);
        jRadioButton.addActionListener(this);
        jRadioButton.setBounds(x, y, z, h);
        jRadioButton.setBackground(Color.WHITE);
        add(jRadioButton);
        return jRadioButton;
    }

}
