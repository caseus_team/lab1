package render;

import enums.Plane;
import models.Polygon;
import models.Vertex;
import transformation.Transformation;
import transformation.projecting.ProjectionOnPlane;

import java.awt.geom.Path2D;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import static enums.Plane.XY;

class Rendering {

    private List<Polygon> shapes;
    private List<Polygon> axis;
    private double width;
    private double height;

    Rendering(double width, double height) {
        this.width = width;
        this.height = height;
        constructShapes();
        constructAxis();
    }

    private void constructAxis() {
        axis = new ArrayList<>();
        axis.add(
          new Polygon(
                  new Vertex(-500, 0, 0),
                  new Vertex(500, 0, 0)
          )
        );
        axis.add(
                new Polygon(
                        new Vertex(0, -500, 0),
                        new Vertex(0, 500, 0)
                )
        );
        axis.add(
                new Polygon(
                        new Vertex(0, 0, -500),
                        new Vertex(0, 0, 500)
                )
        );
    }

    private void constructShapes() {
        shapes = new ArrayList<>();
        //Parallelepiped
        shapes.add(new Polygon(
                new Vertex(0, 0, 0),
                new Vertex(100, 0, 0),
                new Vertex(100, 0, 200),
                new Vertex(0, 0, 200)
        ));
        shapes.add(new Polygon(
                new Vertex(0, 0, 0),
                new Vertex(100, 0, 0),
                new Vertex(100, 150, 0),
                new Vertex(0, 150, 0)
        ));
        shapes.add(new Polygon(
                new Vertex(100, 0, 0),
                new Vertex(100, 0, 200),
                new Vertex(100, 150, 200),
                new Vertex(100, 150, 0)
        ));
        shapes.add(new Polygon(
                new Vertex(0, 0, 200),
                new Vertex(100, 0, 200),
                new Vertex(100, 150, 200),
                new Vertex(0, 150, 200)
        ));
        shapes.add(new Polygon(
                new Vertex(0, 0, 0),
                new Vertex(0, 0, 200),
                new Vertex(0, 150, 200),
                new Vertex(0, 150, 0)
        ));
        shapes.add(new Polygon(
                new Vertex(0, 150, 0),
                new Vertex(100, 150, 0),
                new Vertex(100, 150, 200),
                new Vertex(0, 150, 200)
        ));
        //Tetrahedron
        shapes.add(new Polygon(
                new Vertex(100, 150, 0),
                new Vertex(100, 150, 200),
                new Vertex(50, 200, 100)
        ));
        shapes.add(new Polygon(
                new Vertex(100, 150, 200),
                new Vertex(0, 150, 200),
                new Vertex(50, 200, 100)
        ));
        shapes.add(new Polygon(
                new Vertex(0, 150, 0),
                new Vertex(0, 150, 200),
                new Vertex(50, 200, 100)
        ));
        shapes.add(new Polygon(
                new Vertex(0, 150, 0),
                new Vertex(100, 150, 0),
                new Vertex(50, 200, 100)
        ));
    }

    public void transform(Transformation transformation) {
        shapes.forEach(polygon -> polygon.transform(transformation));
    }

    Stream<Path2D> getAxis() {
        return axis.stream().map(polygon -> polygon.draw(width, height, new ProjectionOnPlane(XY), XY));
    }

    Stream<Path2D> getShapePaths(Transformation transformation, Plane plane) {
        return shapes.stream().map(polygon -> polygon.draw(width, height, transformation, plane));
    }

}
