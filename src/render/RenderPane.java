package render;

import enums.Axis;
import enums.Plane;
import transformation.Moving;
import transformation.Transformation;
import transformation.projecting.ProjectionOnPlane;

import java.awt.*;
import java.awt.geom.Path2D;

import static java.lang.Math.abs;
import static java.lang.Math.sin;

public class RenderPane {

    private int height;
    private int width;
    private Transformation projection;
    private Plane currentPlane;

    private Rendering rendering;
    private Graphics2D g2;

    public RenderPane(int height, int width) {
        this.height = height;
        this.width = width;
        rendering = new Rendering(width, height);
        projection = new ProjectionOnPlane(Plane.XY);
        currentPlane = Plane.XY;
//        doTransform(new ProjectionOnPlane(Plane.XY));
    }

    private void drawPath(Path2D path2D) {
        g2.setColor(Color.BLACK);
        g2.draw(path2D);
    }

    protected void paintComponent(Graphics g) {
        g2 = (Graphics2D) g;
        g2.setColor(Color.GRAY);
        drawAxis();
        g2.setColor(Color.BLACK);
        drawShapes();
    }

    public void doTransform(Transformation transformation) {
        rendering.transform(transformation);
    }

    public void setProjection(Transformation projection) {
        this.projection = projection;
    }

    public void setCurrentPlane(Plane currentPlane) {
        this.currentPlane = currentPlane;
    }

    private void drawAxis() {
        rendering.getAxis().forEach(this::drawPath);
    }

    private void drawShapes() {
        rendering.getShapePaths(projection, currentPlane).forEach(this::drawPath);
    }
}
